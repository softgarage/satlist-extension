(function () {

    if (!window.satList)
        window.satList = {};

    satList.contentScripts = (function () {

        function parsePage() {
            function getSatName() {
                var satNameSelector = "font[face='tahoma'][color='#000099']";
                var satNameContainer = document.querySelector(satNameSelector);
                return satNameContainer ? satNameContainer.innerText : null;
            }

            function getSatUrl() {
                return window.location.href;
            }

            function getSatData() {
                var rowSelector = "table[bordercolor='#3366cc'] tr[bgcolor='#79bcff'], table[bordercolor='#33646cc'] tr[bgcolor='#79bcff']",
                    nodelist = document.querySelectorAll(rowSelector),
                    data = [];

                for (i = 0; i < nodelist.length; i++) {
                    var freqCell = nodelist[i].querySelectorAll('td')[1];
                    var freqCellValue = freqCell.innerText;
                    var freq = parseInt(freqCellValue.slice(0, freqCellValue.indexOf(' ')));
                    var polarr = freqCellValue.match(/\s\w\s/);
                    var pol = polarr[0].trim();
                    var standarr = freqCellValue.match(/DVB-S[(2/)]/);
                    var stand = standarr[0];
                    var roff = stand == 'DVB-S/' ? 1.35 : 1.2;

                    var srCell = nodelist[i].querySelectorAll('td')[2];
                    var srCellValue = srCell.innerText;
                    var srValue = srCellValue.slice(0, srCellValue.indexOf(' '));
                    var sr = parseInt(srValue ? srValue : "0");

                    var BW = Math.round(sr * roff) / 1000;

                    var nameCell = nodelist[i].querySelectorAll('td')[3];
                    var nameCh = nameCell.innerText.trim();

                    data.push({
                        Pol: pol,
                        Freq: freq,
                        SR: sr,
                        Roff: roff,
                        BW: BW,
                        name: nameCh
                    });
                }

                return data;
            }

            var sat = null;
            var satName = getSatName();

            if (satName) {
                sat = {
                    name: satName,
                    url: getSatUrl(),
                    data: getSatData(),
                    date: new Date().getTime()
                }
            }

            return sat;
        }

        return {
            parsePage: parsePage
        }
    })();
})();