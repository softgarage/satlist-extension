function openSatList() {
    chrome.tabs.create({ url: chrome.extension.getURL('satlist.html') });
}

chrome.browserAction.onClicked.addListener(openSatList);

// Handling UI events to call addon's api
chrome.runtime.onMessage.addListener(function (message) {
    switch (message.name) {
        // on add sattelite event 
        case "add.sattelite": {
            satList.storage.addSattelite(message.value)
                .then(function (satList) {
                    chrome.browserAction.setBadgeText({
                        text: satList.length.toString()
                    });
                });
            break;
        }
        // on remove sattelite event 
        case "remove.sattelite": {
            satList.storage.removeSattelite(message.value)
                .then(function (satList) {
                    chrome.browserAction.setBadgeText({
                        text: satList.length.toString()
                    });
                });
            break;
        }
        default: break;
    }
});

satList.storage.getSatList()
    .then(function (satList) {
        chrome.browserAction.setBadgeText({
            text: satList.length.toString()
        });
    });