(function () {
    function createActionButton(className, handler, dataAfter) {
        var btn = document.createElement("button");
        btn.setAttribute("id", "actionButton");
        if (dataAfter) {
            btn.setAttribute("data-after", dataAfter);
        }
        btn.className = className;
        btn.addEventListener("click", handler);
        document.querySelector("body").appendChild(btn);
    }

    function addHandler(e) {
        var currentPageSatData = satList.contentScripts.parsePage();
        chrome.runtime.sendMessage({ name: "add.sattelite", value: currentPageSatData });
        document.getElementById("actionButton").remove();
    }

    function updateHandler(e) {
        var currentPageSatData = satList.contentScripts.parsePage();
        chrome.runtime.sendMessage({ name: "add.sattelite", value: currentPageSatData });

        document.getElementById("actionButton").remove();
    }
    
    satList.storage.getSatList()
        .then(function (storedSatList) {
            var currentPageSat = satList.contentScripts.parsePage();
            
            if (currentPageSat != null) {   // if page contains sat information
                var ind = satList.storage.findInList(storedSatList, currentPageSat.name);

                if (!~ind) {    // there are no such sattelite in list - add button
                    createActionButton("add-button", addHandler);
                } else {
                    var savedSatData = storedSatList[ind];

                    if (((currentPageSat.date - savedSatData.date) / 1000 / 60 / 60 / 24) > 1) {
                        createActionButton("update-button", updateHandler, savedSatData.date);
                    }
                }

            } else {
                console.log("Sat List Extension: No sat data found.");
            }

        });

})();