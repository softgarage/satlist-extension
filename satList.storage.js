(function () {

    if (!window.satList)
        window.satList = {};

    satList.storage = (function () {

        var satListStorageId = "SatList";

        function getIndexOfSatteliteInList(list, satteliteName) {
            var index = -1;

            for (var i = 0; i < list.length; i++) {
                if (list[i].name == satteliteName) {
                    index = i;
                    break;
                }
            }

            return index;
        }

        function getSatList() {
            return new Promise(function (resolve, reject) {
                chrome.storage.local.get(satListStorageId, function (storageObj) {
                    if (storageObj && storageObj[satListStorageId]) {
                        resolve(storageObj[satListStorageId]);
                    } else {
                        resolve([]);
                    }
                });
            });
        }

        function getSattelite(satteliteId) {
            return new Promise(function (resolve, reject) {
                getSatList()
                    .then(function (satList) {
                        var index = getIndexOfSatteliteInList(satList, satteliteId);

                        if (~index) {
                            resolve(satList[index]);
                        } else {
                            reject();
                        }
                    }, function () {
                        reject();
                    });
            });
        }

        function addSattelite(sattelite) {
            return new Promise(function (resolve, reject) {
                getSatList()
                    .then(function (satList) {
                        // add to non-empty satlist
                        var index = getIndexOfSatteliteInList(satList, sattelite.name);
                        if (~index) {
                            satList[index] = sattelite;
                        } else {
                            satList.push(sattelite);
                        }

                        var storageItem = {};
                        storageItem[satListStorageId] = satList;

                        chrome.storage.local.set(storageItem, function () {
                            getSatList()
                                .then(function (satList) {
                                    resolve(satList);
                                }, function () {
                                    reject();
                                })
                        });

                    }, function () {
                        // add to empty satlist
                        var storageItem = {};
                        storageItem[satListStorageId] = [sattelite];

                        chrome.storage.local.set(storageItem, function () {
                            getSatList()
                                .then(function (satList) {
                                    resolve(satList);
                                }, function () {
                                    reject();
                                });
                        });
                    });
            });
        }

        function removeSattelite(satteliteName) {
            return new Promise(function (resolve, reject) {
                getSatList()
                    .then(function (satList) {
                        var index = getIndexOfSatteliteInList(satList, satteliteName);
                        if (~index) {
                            satList.splice(index, 1);
                        }

                        var storageItem = {};
                        storageItem[satListStorageId] = satList;

                        chrome.storage.local.set(storageItem, function () {
                            getSatList()
                                .then(function (satList) {
                                    resolve(satList);
                                })
                        });
                    })
            });
        }

        return {
            getSatList: getSatList,
            getSattelite: getSattelite,
            addSattelite: addSattelite,
            removeSattelite: removeSattelite,
            findInList: getIndexOfSatteliteInList
        }
    })();

})();