var FREQ_START = 3000;
var DEFAULT_DISPLAY_RANGE = 1000;
var TOTAL_DISPLAY_RANGE = 10000;
var centeredAt = 3800;
var scrolled = 300;
var lastScale = scaleValue();

// Returns array of selected values
function polarizationValue() {
    var valuesList = [];
    $("input[name='polarization']:checked").each(function (i, v) {
        valuesList.push($(v).val());
    })
    return valuesList;
}

// Returns int scale factor
function scaleValue() {
    return parseInt($("input[name='scale']:checked").val());
}

// Formats datetime to readable string
function formatDatetime(date) {
    function doubleNumber(number) {
        return number < 10 ? "0" + number.toString() : number.toString();
    }

    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return [date.getDate(),
    monthNames[date.getMonth()],
    date.getFullYear()].join(' ') +
        "  " +
        [doubleNumber(date.getHours()),
        doubleNumber(date.getMinutes()),
        doubleNumber(date.getSeconds())].join(':');
}

function createAndFillSatElement(satData, polarizations, scale) {
    // Creates empty sat element
    function createSatElement(scale) {
        var $satContainer = $("<div class='sat-container'></div>");
        var rullerSteps = 20;

        // Add Sat Info: Name and Date parsed
        var $satInfo = $("<div class='sat-info'></div>");
        var $satName = $("<a class='sat-name'></a>");
        var $satActions = $("<span class='sat-actions'><a class='deleteBtn' href='javascript:void(0);'>Delete</a></span>");
        var $satDate = $("<span class='sat-date' title='Date Parsed'></span>");
        $satInfo
            .append($satName)
            .append($satActions)
            .append($satDate);

        // Add spectrum visualizer
        var $scroller = $("<div class='workload-scroller'></div>");
        var $workload = $("<div class='sat-workload'></div>");
        var workloadWidthValue = TOTAL_DISPLAY_RANGE * scale
        $workload.css("width", workloadWidthValue + "px");
        $scroller
            .append($workload)
            .on("scroll", onScroll);

        // Add ruller
        var $ruller = $("<table class='ruller'><tbody><tr></tr></tbody></table>");
        $ruller = $ruller.first("tr");
        for (var i = 0; i < rullerSteps; i++) {
            var $rullerStep = $("<td></td>");
            $rullerStep.attr("data-mult", i + 1);
            $ruller.append($rullerStep);
        }

        $satContainer
            .append($satInfo)
            .append($scroller)
            .append($ruller);


        return $satContainer;
    }

    //Fills sat element with passed data
    function fillSatElement(element, satData, polarizations, scale) {
        function createSignalElement(signalData, scale) {
            var $signalElement = $("<div class='sat-signal'></div");
            var polarizationColors = {
                'L': "#00B1DB",
                'R': "#FF4B4B",
                'V': "#FFF55C",
                'H': "#CE45FF"
            }
            $signalElement
                .attr("data-pol", signalData.Pol)
                .attr("data-freq", signalData.Freq)
                .attr("data-sr", signalData.SR)
                .attr("data-roff", signalData.Roff)
                .attr("data-bw", signalData.BW)
                .attr("data-name", signalData.name);

            var signalStartValue = ((signalData.Freq - signalData.BW / 2) - FREQ_START) * scale;
            var signalBandwidthValue = signalData.BW * scale;

            $signalElement
                .css("background", polarizationColors[signalData.Pol])
                .css("left", signalStartValue + "px")
                .css("width", signalBandwidthValue + "px");

            return $signalElement;
        }

        var $satElement = $(element);

        // Set name and link
        $satElement.find(".sat-name")
            .text(satData.name)
            .attr("href", satData.url);

        // Set actions behavior
        $satElement.find(".deleteBtn")
            .attr("data-value", satData.name);

        // Set parsed date
        var d = new Date(satData.date);
        $satElement.find(".sat-date").text(" as of " + formatDatetime(d));

        // Set signals 
        var filteredSignals = satData.data.filter(function (signal) {
            return polarizations.indexOf(signal.Pol) >= 0;
        }, polarizations);
        var $signalsWrapper = $satElement.find(".sat-workload");
        for (var i = 0; i < filteredSignals.length; i++) {
            var $signalElement = createSignalElement(filteredSignals[i], scale);
            $signalsWrapper.append($signalElement);
        }
    }

    var $satElement = createSatElement(scale);
    fillSatElement($satElement, satData, polarizations, scale);

    return $satElement;
}

function showSattelites(satList) {
    var $sattelitesWrapper = $("#sattelites");
    var polarizations = polarizationValue();
    var scale = scaleValue();

    // Cleanup container
    $sattelitesWrapper.empty();

    // Fill container in cycle
    for (var i = 0; i < satList.length; i++) {
        var $satElement = createAndFillSatElement(satList[i], polarizations, scale);
        $sattelitesWrapper.append($satElement);
    }
    centerAt(centeredAt);
}

function update() {
    satList.storage.getSatList()
        .then(function (satList) {
            showSattelites(satList);
            updateRulers();
        });
}

//function getSatList() {
//	var belintersat = JSON.parse('{"name":"Belintersat 1 (ChinaSat 15) @ 51.5° East","url":"https://www.flysat.com/belintersat1.php","data":[{"Pol":"R","Freq":3630,"SR":2607,"Roff":1.35,"BW":3.519,"name":"OBN (Oromiya Broadcasting Network)"},{"Pol":"R","Freq":3634,"SR":5207,"Roff":1.35,"BW":7.029,"name":"OBN HD (Oromiya Broadcasting Network)"},{"Pol":"R","Freq":3639,"SR":2275,"Roff":1.2,"BW":2.73,"name":"TiGray TV"},{"Pol":"R","Freq":3645,"SR":4915,"Roff":1.35,"BW":6.635,"name":"South TV"},{"Pol":"R","Freq":3695,"SR":3640,"Roff":1.2,"BW":4.368,"name":"ETV HD Ethiopia"},{"Pol":"R","Freq":3706,"SR":15626,"Roff":1.2,"BW":18.751,"name":"ETV"},{"Pol":"R","Freq":3719,"SR":2607,"Roff":1.35,"BW":3.519,"name":"OBN-TVO"},{"Pol":"R","Freq":3723,"SR":1210,"Roff":1.2,"BW":1.452,"name":"Walta TV"},{"Pol":"R","Freq":3727,"SR":2960,"Roff":1.35,"BW":3.996,"name":"ETV Ethiopia"},{"Pol":"R","Freq":3818,"SR":8000,"Roff":1.35,"BW":10.8,"name":"Challenger"},{"Pol":"R","Freq":3834,"SR":1759,"Roff":1.2,"BW":2.111,"name":"HKMG TV"},{"Pol":"L","Freq":3890,"SR":2295,"Roff":1.2,"BW":2.754,"name":"Kwesé Free Sports Stream"},{"Pol":"L","Freq":3930,"SR":27500,"Roff":1.2,"BW":33,"name":"Vicus-Luxlink info card"},{"Pol":"H","Freq":11090,"SR":30000,"Roff":1.2,"BW":36,"name":"TSTV Africa"},{"Pol":"H","Freq":11130,"SR":30000,"Roff":1.2,"BW":36,"name":"TSTV Africa"},{"Pol":"H","Freq":11170,"SR":30000,"Roff":1.2,"BW":36,"name":"TSTV Africa"},{"Pol":"H","Freq":11230,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"H","Freq":11244,"SR":11457,"Roff":1.2,"BW":13.748,"name":"@SkyEdge II"},{"Pol":"H","Freq":11238,"SR":4200,"Roff":1.2,"BW":5.04,"name":"Data"},{"Pol":"V","Freq":11246,"SR":4100,"Roff":1.2,"BW":4.92,"name":"Data"},{"Pol":"H","Freq":11290,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"H","Freq":11350,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":11365,"SR":4200,"Roff":1.2,"BW":5.04,"name":"Data"},{"Pol":"H","Freq":11410,"SR":0,"Roff":1.2,"BW":0,"name":""}],"date":1552919911062}');
//	var monacosat = JSON.parse('{"name":"TürkmenÄlem/MonacoSat @ 52° East","url":"https://www.flysat.com/TurkmenalemMonacosat.php","data":[{"Pol":"H","Freq":10721,"SR":0,"Roff":1.35,"BW":0,"name":""},{"Pol":"V","Freq":10762,"SR":35000,"Roff":1.2,"BW":42,"name":"Data"},{"Pol":"H","Freq":10794,"SR":40000,"Roff":1.2,"BW":48,"name":"Data"},{"Pol":"H","Freq":10804,"SR":30000,"Roff":1.2,"BW":36,"name":"AIMAG feed"},{"Pol":"V","Freq":10845,"SR":27500,"Roff":1.35,"BW":37.125,"name":"Wide Network Solutions"},{"Pol":"V","Freq":10887,"SR":27500,"Roff":1.35,"BW":37.125,"name":"STN"},{"Pol":"V","Freq":11204,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11215,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11247,"SR":4100,"Roff":1.2,"BW":4.92,"name":"@SkyEdge II"},{"Pol":"V","Freq":11257,"SR":4100,"Roff":1.2,"BW":4.92,"name":"Data"},{"Pol":"V","Freq":11287,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11294,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11304,"SR":7000,"Roff":1.2,"BW":8.4,"name":"Turkmenistan TV feed"},{"Pol":"H","Freq":11305,"SR":30000,"Roff":1.2,"BW":36,"name":"feed"},{"Pol":"V","Freq":11307,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11312,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11318,"SR":4000,"Roff":1.2,"BW":4.8,"name":"Turkmenistan TV feed"},{"Pol":"V","Freq":11386,"SR":20000,"Roff":1.2,"BW":24,"name":"Data"},{"Pol":"V","Freq":11420,"SR":27494,"Roff":1.2,"BW":32.993,"name":"Data"},{"Pol":"V","Freq":12226,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"H","Freq":12229,"SR":3030,"Roff":1.2,"BW":3.636,"name":"Data"},{"Pol":"V","Freq":12264,"SR":27500,"Roff":1.2,"BW":33,"name":"TMT"},{"Pol":"V","Freq":12303,"SR":27500,"Roff":1.2,"BW":33,"name":"TMT"},{"Pol":"H","Freq":12322,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":12341,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":12380,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":12417,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":12430,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"H","Freq":12437,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":12456,"SR":3357,"Roff":1.2,"BW":4.028,"name":"SuperYacht TV HD"},{"Pol":"H","Freq":12476,"SR":30000,"Roff":1.2,"BW":36,"name":"Vicus Luxlink"}],"date":1552920509010}');
//	var somesat = JSON.parse('{"name":"Al Yah 1 @ 52.5 East","url":"https://www.flysat.com/alyah1.php","data":[{"Pol":"R","Freq":3570,"SR":25000,"Roff":1.2,"BW":30,"name":"Data"},{"Pol":"R","Freq":3962,"SR":2375,"Roff":1.2,"BW":2.85,"name":"Data"},{"Pol":"L","Freq":3970,"SR":36300,"Roff":1.2,"BW":43.56,"name":"Data"},{"Pol":"R","Freq":3979,"SR":3610,"Roff":1.2,"BW":4.332,"name":"Data"},{"Pol":"R","Freq":3982,"SR":3709,"Roff":1.2,"BW":4.451,"name":"Data"},{"Pol":"R","Freq":4005,"SR":4744,"Roff":1.2,"BW":5.693,"name":"Data"},{"Pol":"L","Freq":4020,"SR":3610,"Roff":1.2,"BW":4.332,"name":"Data"},{"Pol":"L","Freq":4025,"SR":3610,"Roff":1.2,"BW":4.332,"name":"Data"},{"Pol":"L","Freq":4035,"SR":3610,"Roff":1.2,"BW":4.332,"name":"Data"},{"Pol":"L","Freq":4040,"SR":3610,"Roff":1.2,"BW":4.332,"name":"Data"},{"Pol":"L","Freq":4045,"SR":3610,"Roff":1.2,"BW":4.332,"name":"Data"},{"Pol":"R","Freq":4063,"SR":11880,"Roff":1.2,"BW":14.256,"name":"Data"},{"Pol":"L","Freq":4162,"SR":2400,"Roff":1.2,"BW":2.88,"name":"Data"},{"Pol":"V","Freq":11727,"SR":27500,"Roff":1.2,"BW":33,"name":"Data"},{"Pol":"H","Freq":11747,"SR":27500,"Roff":1.2,"BW":33,"name":"Saudi Broadcasting Authority"},{"Pol":"V","Freq":11766,"SR":27500,"Roff":1.35,"BW":37.125,"name":"NorthTelecom"},{"Pol":"H","Freq":11785,"SR":27500,"Roff":1.2,"BW":33,"name":"YahLive 12"},{"Pol":"H","Freq":11823,"SR":27500,"Roff":1.2,"BW":33,"name":"Saudi Broadcasting Authority"},{"Pol":"V","Freq":11843,"SR":0,"Roff":1.35,"BW":0,"name":""},{"Pol":"H","Freq":11861,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"V","Freq":11881,"SR":27500,"Roff":1.35,"BW":37.125,"name":"GEM Group"},{"Pol":"H","Freq":11900,"SR":27500,"Roff":1.35,"BW":37.125,"name":"NorthTelecom"},{"Pol":"H","Freq":11938,"SR":27500,"Roff":1.35,"BW":37.125,"name":"YahLive 6"},{"Pol":"V","Freq":11958,"SR":27500,"Roff":1.35,"BW":37.125,"name":"NorthTelecom"},{"Pol":"H","Freq":11977,"SR":27500,"Roff":1.2,"BW":33,"name":"YahLive 1"},{"Pol":"V","Freq":11996,"SR":27500,"Roff":1.35,"BW":37.125,"name":"YahLive 2"},{"Pol":"H","Freq":12015,"SR":27500,"Roff":1.2,"BW":33,"name":"YahLive 5"},{"Pol":"V","Freq":12034,"SR":27500,"Roff":1.2,"BW":33,"name":"GEM Group"},{"Pol":"H","Freq":12054,"SR":27500,"Roff":1.2,"BW":33,"name":""},{"Pol":"V","Freq":12073,"SR":27495,"Roff":1.2,"BW":32.994,"name":"GMD (General Media and Data)"},{"Pol":"H","Freq":12092,"SR":27500,"Roff":1.2,"BW":33,"name":"YahLive 11"},{"Pol":"V","Freq":12111,"SR":0,"Roff":1.35,"BW":0,"name":""},{"Pol":"H","Freq":12130,"SR":27500,"Roff":1.2,"BW":33,"name":"YahLive 9"},{"Pol":"V","Freq":12149,"SR":0,"Roff":1.2,"BW":0,"name":""},{"Pol":"H","Freq":12168,"SR":27500,"Roff":1.2,"BW":33,"name":"YahLive 10"},{"Pol":"V","Freq":12188,"SR":0,"Roff":1.2,"BW":0,"name":""}],"date":1552920961892}');

//	return [belintersat, monacosat, somesat];
//}

function getCenterByScroll(scrollValue) {
    var scale = scaleValue();
    var newCenterValue = FREQ_START + (scrollValue / scale + DEFAULT_DISPLAY_RANGE / scale / 2);

    return newCenterValue;
}

function getScrollByCenter(centerValue) {
    var scale = scaleValue();
    var newScrollValue = ((centerValue - FREQ_START) * scale) - (DEFAULT_DISPLAY_RANGE / 2)

    if (newScrollValue < 0) {
        newScrollValue = 0;
        centeredAt = getCenterByScroll(newScrollValue);
    }
    if (newScrollValue > TOTAL_DISPLAY_RANGE * scale - DEFAULT_DISPLAY_RANGE) {
        newScrollValue = TOTAL_DISPLAY_RANGE * scale - DEFAULT_DISPLAY_RANGE;
        centeredAt = getCenterByScroll(newScrollValue);
    }

    return newScrollValue;
}

function centerAt(value) {
    centeredAt = value;
    var oldScroll = $('.workload-scroller').scrollLeft();
    var newScroll = getScrollByCenter(value);
    if (oldScroll != newScroll) {
        $('.workload-scroller').scrollLeft(newScroll);
    }
}

function shiftMhz(value) {
    centerAt(centeredAt + value);
}

function updateRulers() {
    var scale = scaleValue();
    var scales = 20;
    for (var i = 0; i < scales; i++) {
        var $rullerCell = $(".sat-container .ruller td:nth-of-type(" + (i + 1) + ")");
        var currentDisplayRange = DEFAULT_DISPLAY_RANGE / scale;
        var rullerStartValue = centeredAt - currentDisplayRange / 2;
        var rullerCellValue = Math.floor(rullerStartValue + i * (currentDisplayRange / scales));
        $rullerCell.text(rullerCellValue + ' MHz');
    }
}

function onScroll(e) {
    var scale = scaleValue();
    if (scale == lastScale) {
        centeredAt = getCenterByScroll($(this).scrollLeft());

        var scroll = getScrollByCenter(centeredAt);
        $('.workload-scroller').not(this).scrollLeft(scroll);
        updateRulers();

        setTimeout(function () {
            var sc = getCenterByScroll($('.workload-scroller').scrollLeft());
            var step = (100 / scale);

            if (sc % 100 != 0) {
                centerAt((Math.round(sc / step) * step));
                updateRulers();
            }
        }, 500);
    } else {
        lastScale = scale;
    }
}

$(".filters input").change(update);
$(".shiftBtn").click(function (e) {
    shiftMhz(parseInt($(this).data("value")));
});
$(".scrollBtn").click(function (e) {
    centerAt(parseInt($(this).data("value")));
});

$("body").on("click", ".sat-actions .deleteBtn", function (e) {
    var satNameToDelete = $(this).attr("data-value");
    chrome.runtime.sendMessage({ name: "remove.sattelite", value: satNameToDelete });
    $(this).parents(".sat-container").remove();
});

update();